# fetchlyrics.py
A python script to fetch lyrics and annotations from genius.com


## Controls
- left and right arrows move through annotations
- up and down arrows scroll
- enter selects annotation
- r re-fetches lyrics


## Todo
- Code cleanup
