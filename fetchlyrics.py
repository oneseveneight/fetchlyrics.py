#!/usr/bin/python3
import curses
import requests
import json
import sys
import re
from curses import wrapper
from html.parser import HTMLParser
from mpris2 import get_players_uri
from mpris2 import Player
from requests.utils import quote


class LyricsParser(HTMLParser):
    lyrics_div = 0
    lyrics_p = 0
    lyrics_a = ''
    line_num = 0
    lyrics_text = []

    def handle_starttag(self, tag, attrs):
        if tag == 'div':
            for name, value in attrs:
                if name == 'class' and value == 'lyrics':
                    LyricsParser.lyrics_div = 1
        elif tag == 'p' and LyricsParser.lyrics_div == 1:
            LyricsParser.lyrics_p = 1
        elif tag == 'a':
            for name, value in attrs:
                if name == 'annotation-fragment':
                    LyricsParser.lyrics_a = value

    def handle_data(self, data):
        if (LyricsParser.lyrics_p == 1
           and LyricsParser.lyrics_div == 1
           and data != ' '):
            LyricsParser.line_num += 1
            LyricsParser.lyrics_text.append([data, ''])

    def handle_endtag(self, tag):
        if (tag == 'a'
           and len(LyricsParser.lyrics_a) != 0
           and LyricsParser.lyrics_div != 0):
            LyricsParser.lyrics_text[LyricsParser.line_num - 1][1] \
                = LyricsParser.lyrics_a

        elif tag == 'p' and LyricsParser.lyrics_p == 1:
            LyricsParser.lyrics_p = 0

        elif tag == 'div' and LyricsParser.lyrics_div == 1:
            LyricsParser.lyrics_div = 0


def getLyricsUrl():
    try:
        player_uri = next(get_players_uri())
        player = Player(dbus_interface_info={'dbus_uri': player_uri})
        trackartist_tracktitle = (player.Metadata[player.Metadata.ARTIST][0]
                                  + " - "
                                  + player.Metadata[player.Metadata.TITLE])
    except StopIteration:
        try:
            import subprocess

            cmd = r"""wmctrl -l -p | cut -c -10| sed -e 's/0x/xprop -id 0x/g'|
            sh | grep 'WM_CLASS(STRING) = "spotify", "Spotify"' -A 2|
            grep '_NET_WM_NAME(UTF8_STRING)'|cut -c 29-| head -n 1|
            sed -e 'y/āáǎàēéěèīíǐìōóǒòūúǔùǖǘǚǜ/aaaaeeeeiiiioooouuuuuuuu/'|
            tr -d '\200-\377'"""

            ps = subprocess.Popen(cmd,
                                  shell=True,
                                  stdout=subprocess.PIPE,
                                  stderr=subprocess.STDOUT)

            trackartist_tracktitle = str(ps.communicate()[0])
            if '-' not in trackartist_tracktitle:
                return -2
        except ImportError:
            return -1

        trackartist_tracktitle = re.search('".*"',
                                           trackartist_tracktitle).group(0)

    lyrics_url = trackartist_tracktitle.lower()
    lyrics_url = re.sub('.feat\..*?[\]\)]', '', lyrics_url)
    lyrics_url = re.sub(r"""['"\(\)\[\]\/\\.,\*!]""", '', lyrics_url)
    lyrics_url = re.sub(' +', ' ', lyrics_url)
    lyrics_url = re.sub(' ', '-', lyrics_url)
    lyrics_url = lyrics_url + '-lyrics'
    lyrics_url = re.sub('-+', '-', lyrics_url)
    lyrics_url = 'https://genius.com/' + lyrics_url

    return lyrics_url


found_tags = ''
annotation_str = ''


def annotationParse(annotation_data):
    global found_tags
    global annotation_str

    found_tags += annotation_data['tag']
    if annotation_data['tag'] == 'blockquote':
        annotation_str += '\n❝\n'

    if re.search('blockquotep.*p', found_tags) is not None:
        annotation_str += '\n❞\n'
        found_tags = ''

    for i in range(len(annotation_data['children'])):
        if type(annotation_data['children'][i]) is str:
            annotation_str += annotation_data['children'][i] + ' '
        elif type(annotation_data['children'][i]) is dict:
            if 'children' in annotation_data['children'][i]:
                annotationParse(annotation_data['children'][i])

    return annotation_str


def drawAnnotation(annotation_id, window_name):
    window_name.clear()
    window_name.addstr("Loading annotation...")
    window_name.refresh()

    key = 'fCg8yPiYpjMeScN-blH27nC1qY2C4Nia27tP4B_iM3EyEJgObAudBhpPdHj2wnwL'
    headers = {'Authorization': 'Bearer ' + key}
    url = 'https://api.genius.com/annotations/' + str(annotation_id)
    annotation_json = json.loads(requests.get(url, headers=headers).text)
    annotation_text = ''

    window_name.clear()
    window_name.refresh()

    annotation_text = annotationParse(annotation_json
                                      ['response']
                                      ['annotation']
                                      ['body']
                                      ['dom'])
    window_name.addstr(str(annotation_text))
    window_name.refresh()


def drawLyrics(window_name):
    global selected_annotaion_id
    lyrics_list = LyricsParser.lyrics_text
    window_name.clear()

    lines = 0
    attr = 0
    annotation_num = 0

    for i in range(scroll, len(lyrics_list)):
        if lyrics_list[i][1] != '':
            if annotation_num == selected_annotaion:
                attr = curses.A_UNDERLINE
                selected_annotaion_id = lyrics_list[i][1]
            else:
                attr = curses.A_STANDOUT
            annotation_num += 1
        else:
            attr = 0

        if '\n' in lyrics_list[i][0]:
            lines += 1

        if lines == maxY:
            break

        window_name.addstr(lyrics_list[i][0], attr)

    annotation_num = 0
    window_name.refresh()


def loadLyrics(window_name):
    window_name.clear()
    window_name.addstr("Loading lyrics...")
    window_name.refresh()
    lyrics_url = getLyricsUrl()

    if not isinstance(lyrics_url, str):
        errstr = ''
        if lyrics_url == -1:
            errstr = 'a library is missing'
        elif lyrics_url == -2:
            errstr = 'Spotify is not open, or no music is currently playing'
        window_name.clear()
        window_name.addstr('Error when reading song data from window title: '
                           + errstr)
        window_name.refresh()
        window_name.getch()
        return -1

    response = requests.get(lyrics_url)
    if (str(response.status_code)[0] == '4'
       or str(response.status_code)[0] == '5'):
        window_name.clear()
        window_name.addstr('Could not get lyrics from '
                           + lyrics_url
                           + ': Error '
                           + str(response.status_code))
        window_name.refresh()
        window_name.getch()
        return -1

    lyrics_html = response.text
    LyricsParser().feed(lyrics_html)
    drawLyrics(window_name)
    return 0


def main(stdscr):
    global maxY
    global scroll
    global selected_annotaion
    global annotation_str

    maxY, scroll, selected_annotaion = 0, 0, 0

    curses.use_default_colors()
    curses.curs_set(0)
    curses.qiflush(True)
    stdscr.timeout(0)
    stdscr.idlok(True)
    stdscr.scrollok(True)
    stdscr.nodelay(False)

    maxY = stdscr.getmaxyx()[0]
    viewing_annotation = False

    if loadLyrics(stdscr) != 0:
        return -1

    while True:
        ch = stdscr.getch()
        if ch == ord('q'):
            break
        elif ch == curses.KEY_UP:
            curses.flushinp()
            if scroll > 0:
                scroll -= 2
                drawLyrics(stdscr)
        elif ch == curses.KEY_DOWN:
            curses.flushinp()
            if curses.getsyx()[0] == maxY - 1:
                scroll += 2
            drawLyrics(stdscr)
        elif ch == curses.KEY_LEFT:
            curses.flushinp()
            selected_annotaion -= 1
            drawLyrics(stdscr)
        elif ch == curses.KEY_RIGHT:
            curses.flushinp()
            selected_annotaion += 1
            drawLyrics(stdscr)
        elif ch == curses.KEY_ENTER or ch == 10:
            curses.flushinp()
            viewing_annotation = not viewing_annotation
            if viewing_annotation is True:
                annotation_str = ''
                drawAnnotation(selected_annotaion_id, stdscr)
            else:
                drawLyrics(stdscr)
        elif ch == ord('r'):
            LyricsParser.lyrics_text = []
            loadLyrics(stdscr)


curses.wrapper(main)
